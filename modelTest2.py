
from HMM import HMM
from HMM2 import HMM2
from myParser import collect_sets
import matplotlib.pyplot as plt
import numpy as np


def splitData(data, numOfChunks):
	n = int(len(data) / numOfChunks)
	return [data[i:i + n] for i in range(0, len(data), n)]

def extractXandY(trainData):
	x = [tup[0] for tup in trainData]
	y = [tup[1] for tup in trainData]
	return x, y

def getData(numOfChunks, maxSentences):
	sets, xvals, yvals = collect_sets('data_split.gz', k=10, max_sentences=maxSentences)

	x_train = []
	y_train = []

	trainData = splitData(sets['train'], numOfChunks)

	for i in range(numOfChunks):
		x, y = extractXandY(trainData[i])
		x_train.append(x)
		y_train.append(y)

	x_test, y_test = extractXandY(sets['test'])

	return x_train, y_train, x_test, y_test, xvals, yvals


numOfChunks = 5
maxSentences = 300
rate = np.array([1e-1, 1e-1, 1e-2, 1e-3, 1e-4])

x_train, y_train, x_test, y_test, xvals, yvals = getData(numOfChunks, maxSentences)

error = np.zeros(numOfChunks)

hmm = HMM2(xvals, yvals)

for i in range(numOfChunks):
	hmm.perceptron(x_train[i], y_train[i], rate[0])
	error[i] = hmm.checkError(x_test, y_test)

plt.figure()
plt.plot(np.arange(18, 108, 18), error, linewidth=2.0, marker='D', markersize=5, mfc='r')
plt.xlabel('train data %', fontsize=14)
plt.ylabel('error on test set', fontsize=14)
plt.title('Vanilla Perceptron', fontsize=20)
plt.show()
