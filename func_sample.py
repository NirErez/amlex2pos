
def sample ( Ns , xvals=[] , yvals=[] , t=[] , e=[] ):
    """
    sample sequences from the model .
    : param Ns : a vector with desired sample lengths , a sample is generated per
    entry in the vector , with corresponding length .
    : param xvals : the possible values for x variables , ordered as in t , and e
    : param yvals : the possible values for y variables , ordered as in e
    : param t : the transition distributions of the model
    : param e : the emission distributions of the model
    : return : x , y - two iterables describing the sampled sequences .
    """

    """
    #example:
    yvals = ['START','hello','is','it','me','youre','looking','for','from','the','other']
    xvals = ['START','A','B','C','D','E','F']

    t = [[0,0.1,0.2,0.05,0.3,0.3,0.05],
         [0,0.1,0.2,0.05,0.3,0.3,0.05],
         [0,0.1,0.2,0.05,0.3,0.3,0.05],
         [0,0.1,0.2,0.05,0.3,0.3,0.05],
         [0,0.1,0.2,0.05,0.3,0.3,0.05],
         [0,0.1,0.2,0.05,0.3,0.3,0.05],
         [0,0.1,0.2,0.05,0.3,0.3,0.05]]
    e = [[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ],
         [0, 0.1, 0.15, 0.05, 0.1, 0.2, 0.05, 0.1, 0.15, 0.05, 0.05],
         [0, 0.1, 0.15, 0.05, 0.1, 0.2, 0.05, 0.1, 0.15, 0.05, 0.05],
         [0, 0.1, 0.15, 0.05, 0.1, 0.2, 0.05, 0.1, 0.15, 0.05, 0.05],
         [0, 0.1, 0.15, 0.05, 0.1, 0.2, 0.05, 0.1, 0.15, 0.05, 0.05],
         [0, 0.1, 0.15, 0.05, 0.1, 0.2, 0.05, 0.1, 0.15, 0.05, 0.05],
         [0, 0.1, 0.15, 0.05, 0.1, 0.2, 0.05, 0.1, 0.15, 0.05, 0.05]]
    """

    x = []
    y = []
    tagind = numpy.random.choice(numpy.arange(0, len(xvals)), p=t[0])
    for i, samLen in enumerate(Ns):
        x.append([])
        y.append([])

        for elem in range(samLen):
            print(elem)
            x[i].append(xvals[tagind])
            word = numpy.random.choice(yvals, p=e[tagind])
            y[i].append(word)
            tagind = numpy.random.choice(numpy.arange(0, len(xvals)), p=t[tagind])

    return x, y



