# coding=utf-8
import numpy as np

class HMM(object):
	def __init__(self, xvals, yvals):
		xvals = list(xvals)
		xvals.sort()
		xvals.insert(0, 'START')
		self.xvals = xvals
		self.sizeX = len(xvals)

		yvals = list(yvals)
		yvals.sort()
		yvals.insert(0, 'START')
		self.yvals = yvals
		self.sizeY = len(yvals)

		self.tagToIndex = {xvals[i]: i for i in range(len(xvals))}
		self.wordToIndex = {yvals[i]: i for i in range(len(yvals))}

		self.w = -np.random.rand(self.sizeX * (self.sizeX + self.sizeY))

	def mle(self, x, y):
		"""
		Calculate the maximum likelihood estimators for the transition and
		emission distributions , in the multinomial HMM case .
		: param x : an iterable over sequences of POS tags
		: param y : a matching iterable over sequences of words
		: return : a tuple (t , e ) , with
		t . shape = (| val ( X )| ,| val ( X )|) , and
		e . shape = (| val ( X )| ,| val ( Y )|)
		"""
		sizeX = len(self.xvals)
		sizeY = len(self.yvals)

		# initialize matrices
		t = np.zeros([sizeX, sizeX])
		e = np.zeros([sizeX, sizeY])

		# fill matrices
		for i, X in enumerate(x):
			t[0, self.tagToIndex[X[0]]] += 1		# initial probability
			for j in range(len(X) - 1):
				t[self.tagToIndex[X[j]], self.tagToIndex[X[j+1]]] += 1	# transition prob
				e[self.tagToIndex[X[j]], self.wordToIndex[y[i][j]]] += 1 # emission prob

		e[0,0] = 1 	# START token is always START token

		# replace 0 rows with 1 rows, meaning uniform probability for that row
		sum_t = np.sum(t, 1)
		sum_e = np.sum(e, 1)

		t[sum_t == 0, :] = 1
		e[sum_e == 0, :] = 1

		t = (t.T / np.sum(t, 1)).T
		e = (e.T / np.sum(e, 1)).T

		return t, e


	def viterbi(self, y, t, e):
		"""
		Calculate the maximum a - posteriori assignment of x ’s .
		: param y : a sequence of words
		: param suppx : the support of x ( what values it can attain )
		: param t : the transition distributions of the model
		: param e : the emission distributions of the model
		: return : xhat , the most likely sequence of hidden states ( parts of speech ).
		"""
		lenY = len(y)
		sizeX = len(self.xvals)
		v = np.zeros([lenY, sizeX])
		prev_x = np.zeros_like(v, np.int)

		# initialization
		temp = t[0, :] * e[:, self.wordToIndex[y[0]]]
		temp[temp == 0] = 1e-5
		v[0, :] = temp

		# fill the table
		for i in range(1, len(y)):
			for j in range(sizeX):
				probY = e[j, self.wordToIndex[y[i]]]
				if probY == 0:
					probY = 1e-5
				prob = probY * t[:, j] * v[i - 1, :]
				prev_x[i, j] = np.argmax(prob)
				v[i, j] = prob[prev_x[i, j]]

		xhatIndices = np.zeros(lenY, dtype=np.int)
		xhatIndices[-1] = np.argmax(v[-1, :])
		for i in reversed(range(lenY - 1)):
			xhatIndices[i] = prev_x[i + 1, xhatIndices[i + 1]]

		tempNpArray = np.array(self.xvals)
		return tempNpArray[xhatIndices].tolist()


	def viterbi2(self, y):
		"""
		Calculate the assignment of x that obtains the maximum log - linear score .
		: param y : a sequence of words
		: param suppx : the support of x ( what values it can attain )
		: param phi : a mapping from ( x_t , x_ { t +1} , y_ {1.. t +1} to indices of w
		: param w : the linear model
		: return : xhat , the most likely sequence of hidden states ( parts of speech ).
		"""
		lenY = len(y)
		sizeX = len(self.xvals)
		v = np.zeros([lenY, sizeX])
		prev_x = np.zeros_like(v, dtype=np.int)

		# initialization
		for i, tag in enumerate(self.xvals):
			v[0, i] = np.exp(np.sum(self.w[self.phi('START', tag, y[0])]))
			if v[0, i] == 0:
				v[0, i] = 1e-5

		# fill the table
		prob = np.zeros(sizeX)
		for i in range(1, len(y)):
			for j in range(sizeX):
				for k in range(sizeX):
					prob[k] = np.exp(np.sum(self.w[self.phi(self.xvals[k], self.xvals[j], y[i])])) * v[i-1, k]
					if prob[k] == 0:
						prob[k] = 1e-5
				prev_x[i, j] = np.argmax(prob)
				v[i, j] = prob[prev_x[i,j]]

		xhatIndices = np.zeros(lenY, dtype=np.int)
		xhatIndices[-1] = np.argmax(v[-1, :])
		for i in reversed(range(lenY - 1)):
			xhatIndices[i] = prev_x[i + 1, xhatIndices[i + 1]]

		tempNpArray = np.array(self.xvals)
		return tempNpArray[xhatIndices]


	def phi(self, x0, x1, y1):

		x0_index = self.tagToIndex[x0]
		x1_index = self.tagToIndex[x1]
		y_index = self.wordToIndex[y1]

		sizeX = len(self.xvals)
		sizeY = len(self.yvals)

		weightsIndices = [HMM.convertIndices(x0_index, x1_index, [sizeX, sizeX]),
						  HMM.convertIndices(x1_index, y_index, [sizeX, sizeY]) + sizeX * sizeX]

		return weightsIndices

	@staticmethod
	def convertIndices(r, c, shape):
		return r * shape[1] + c


	def logLikelihood(self, x, y, t, e):
		"""
		Calculate the loglikelihood of x, y according to t, e
		:param x: iterable of pos sentences
		:param y: iterable of word sentences
		:param t: the transition probability
		:param e: the emission probability
		:return: the loglikelihood
		"""
		ll = 0

		for i, X in enumerate(x):
			for j, p in enumerate(X):
				if j != len(X) - 1:
					prob = t[self.tagToIndex[X[j]], self.tagToIndex[X[j + 1]]]
					if prob > 0:
						lp = np.log(prob)
						ll += lp
				prob = e[self.tagToIndex[X[j]], self.wordToIndex[y[i][j]]]
				if prob > 0:
					lp = np.log(prob)
					ll += lp

		return ll


	def sample(self, Ns, t=[], e=[]):
		"""
		sample sequences from the model.
		: param Ns : a vector with desired sample lengths , a sample is generated per
		entry in the vector , with corresponding length .
		: param xvals : the possible values for x variables , ordered as in t , and e
		: param yvals : the possible values for y variables , ordered as in e
		: param t : the transition distributions of the model
		: param e : the emission distributions of the model
		: return : x , y - two iterables describing the sampled sequences .
		"""

		x = []
		y = []
		tagind = np.random.choice(np.arange(0, len(self.xvals)), p=t[0])
		for i, samLen in enumerate(Ns):
			x.append([])
			y.append([])

			for elem in range(samLen):
				print(elem)
				x[i].append(self.xvals[tagind])
				word = np.random.choice(self.yvals, p=e[tagind])
				y[i].append(word)
				tagind = np.random.choice(np.arange(0, len(self.xvals)), p=t[tagind])

		return x, y


	def perceptron(self, X, Y, rate):
		"""
		Find w that maximizes the log - linear score
		: param X : POS tags for sentences ( iterable of lists of elements in suppx )
		: param Y : words in respective sentences ( iterable of lists of words in suppy )
		: param suppx : the support of x ( what values it can attain )
		: param suppy : the support of y ( what values it can attain )
		: param phi : a mapping from ( None | x_1 , x_2 , y_2 to indices of w
		: param w0 : initial model
		: param rate : rate of learning
		: return : w , a weight vector for the log - linear model features .
		"""
		for i, x in enumerate(X):
			if i % 100 == 0:
				print(i)
			xhat = self.viterbi2(Y[i])
			for j in range(len(x)):
				if j == 0:
					self.w[self.phi('START', x[j], Y[i][j])] += rate
					self.w[self.phi('START', xhat[j], Y[i][j])] -= rate
				else:
					self.w[self.phi(x[j-1], x[j], Y[i][j])] += rate
					self.w[self.phi(xhat[j-1], xhat[j], Y[i][j])] -= rate

		self.w /= len(X)


	def checkError(self, x_test, y_test):
		totalSize = 0
		errors = 0
		for i in range(len(x_test)):
			x_pred = self.viterbi2(y_test[i])
			errors += (x_pred != np.array(x_test[i])).sum()
			totalSize += len(x_test[i])

		print(errors / totalSize)
		return	errors / totalSize
