# coding=utf-8
import numpy as np

class HMM2(object):
	def __init__(self, xvals, yvals):
		xvals = list(xvals)
		xvals.sort()
		xvals.insert(0, 'START')
		self.xvals = xvals
		self.sizeX = len(xvals)

		yvals = list(yvals)
		yvals.sort()
		yvals.insert(0, 'START')
		self.yvals = yvals
		self.sizeY = len(yvals)

		self.tagToIndex = {xvals[i]: i for i in range(len(xvals))}
		self.wordToIndex = {yvals[i]: i for i in range(len(yvals))}

		self.sizeW = self.sizeX * (self.sizeX + self.sizeY)

		self.suffix1 = []
		self.suffix2 = []
		self.suffix3 = []
		self.suffix4 = []

		with open('suffix') as suffs:
			for suff in suffs:
				suff = suff.rstrip('\n')
				if len(suff) == 1:
					self.suffix1.append(suff)
				elif len(suff) == 2:
					self.suffix2.append(suff)
				elif len(suff) == 3:
					self.suffix3.append(suff)
				elif len(suff) == 4:
					self.suffix4.append(suff)


		self.sizeSuff1 = len(self.suffix1)
		self.sizeSuff2 = len(self.suffix2)
		self.sizeSuff3 = len(self.suffix3)
		self.sizeSuff4 = len(self.suffix4)

		self.w = -np.random.rand(self.sizeW + self.sizeSuff1 + self.sizeSuff2 + self.sizeSuff3 + self.sizeSuff4)


	def viterbi2(self, y):
		"""
		Calculate the assignment of x that obtains the maximum log - linear score .
		: param y : a sequence of words
		: param suppx : the support of x ( what values it can attain )
		: param phi : a mapping from ( x_t , x_ { t +1} , y_ {1.. t +1} to indices of w
		: param w : the linear model
		: return : xhat , the most likely sequence of hidden states ( parts of speech ).
		"""
		lenY = len(y)
		sizeX = len(self.xvals)
		v = np.zeros([lenY, sizeX])
		prev_x = np.zeros_like(v, dtype=np.int)

		# initialization
		for i, tag in enumerate(self.xvals):
			v[0, i] = np.exp(np.sum(self.w[self.phi('START', tag, y[0])]))
			if v[0, i] == 0:
				v[0, i] = 1e-5

		# fill the table
		prob = np.zeros(sizeX)
		for i in range(1, len(y)):
			for j in range(sizeX):
				for k in range(sizeX):
					prob[k] = np.exp(np.sum(self.w[self.phi(self.xvals[k], self.xvals[j], y[i])])) * v[i-1, k]
					if prob[k] == 0:
						prob[k] = 1e-5
				prev_x[i, j] = np.argmax(prob)
				v[i, j] = prob[prev_x[i,j]]

		xhatIndices = np.zeros(lenY, dtype=np.int)
		xhatIndices[-1] = np.argmax(v[-1, :])
		for i in reversed(range(lenY - 1)):
			xhatIndices[i] = prev_x[i + 1, xhatIndices[i + 1]]

		tempNpArray = np.array(self.xvals)
		return tempNpArray[xhatIndices]


	def phi(self, x0, x1, y1):

		x0_index = self.tagToIndex[x0]
		x1_index = self.tagToIndex[x1]
		y_index = self.wordToIndex[y1]

		sizeX = len(self.xvals)
		sizeY = len(self.yvals)

		weightsIndices = [HMM2.convertIndices(x0_index, x1_index, [sizeX, sizeX]),
						  HMM2.convertIndices(x1_index, y_index, [sizeX, sizeY]) + sizeX * sizeX]


		shift = self.sizeW
		suff = y1[-1:]
		if suff in self.suffix1:
			weightsIndices.append(shift + self.suffix1.index(suff))

		shift += self.sizeSuff1
		suff = y1[-2:]
		if suff in self.suffix2:
			weightsIndices.append(shift + self.suffix2.index(suff))

		shift += self.sizeSuff2
		suff = y1[-3:]
		if suff in self.suffix3:
			weightsIndices.append(shift + self.suffix3.index(suff))

		shift += self.sizeSuff3
		suff = y1[-4:]
		if suff in self.suffix4:
			weightsIndices.append(shift + self.suffix4.index(suff))

		return weightsIndices

	@staticmethod
	def convertIndices(r, c, shape):
		return r * shape[1] + c


	def logLikelihood(self, x, y, t, e):
		"""
		Calculate the loglikelihood of x, y according to t, e
		:param x: iterable of pos sentences
		:param y: iterable of word sentences
		:param t: the transition probability
		:param e: the emission probability
		:return: the loglikelihood
		"""
		ll = 0

		for i, X in enumerate(x):
			for j, p in enumerate(X):
				if j != len(X) - 1:
					prob = t[self.tagToIndex[X[j]], self.tagToIndex[X[j + 1]]]
					if prob > 0:
						lp = np.log(prob)
						ll += lp
				prob = e[self.tagToIndex[X[j]], self.wordToIndex[y[i][j]]]
				if prob > 0:
					lp = np.log(prob)
					ll += lp

		return ll


	def sample(self, Ns, t=[], e=[]):
		"""
		sample sequences from the model.
		: param Ns : a vector with desired sample lengths , a sample is generated per
		entry in the vector , with corresponding length .
		: param xvals : the possible values for x variables , ordered as in t , and e
		: param yvals : the possible values for y variables , ordered as in e
		: param t : the transition distributions of the model
		: param e : the emission distributions of the model
		: return : x , y - two iterables describing the sampled sequences .
		"""

		x = []
		y = []
		tagind = np.random.choice(np.arange(0, len(self.xvals)), p=t[0])
		for i, samLen in enumerate(Ns):
			x.append([])
			y.append([])

			for elem in range(samLen):
				print(elem)
				x[i].append(self.xvals[tagind])
				word = np.random.choice(self.yvals, p=e[tagind])
				y[i].append(word)
				tagind = np.random.choice(np.arange(0, len(self.xvals)), p=t[tagind])

		return x, y


	def perceptron(self, X, Y, rate):
		"""
		Find w that maximizes the log - linear score
		: param X : POS tags for sentences ( iterable of lists of elements in suppx )
		: param Y : words in respective sentences ( iterable of lists of words in suppy )
		: param suppx : the support of x ( what values it can attain )
		: param suppy : the support of y ( what values it can attain )
		: param phi : a mapping from ( None | x_1 , x_2 , y_2 to indices of w
		: param w0 : initial model
		: param rate : rate of learning
		: return : w , a weight vector for the log - linear model features .
		"""
		for i, x in enumerate(X):
			if i % 100 == 0:
				print(i)
			xhat = self.viterbi2(Y[i])
			for j in range(len(x)):
				if j == 0:
					self.w[self.phi('START', x[j], Y[i][j])] += rate
					self.w[self.phi('START', xhat[j], Y[i][j])] -= rate
				else:
					self.w[self.phi(x[j-1], x[j], Y[i][j])] += rate
					self.w[self.phi(xhat[j-1], xhat[j], Y[i][j])] -= rate

		self.w /= len(X)


	def checkError(self, x_test, y_test):
		totalSize = 0
		errors = 0
		for i in range(len(x_test)):
			x_pred = self.viterbi2(y_test[i])
			errors += (x_pred != np.array(x_test[i])).sum()
			totalSize += len(x_test[i])

		print(errors / totalSize)
		return	errors / totalSize
