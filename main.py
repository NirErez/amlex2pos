
from HMM import HMM
from myParser import collect_sets
import matplotlib.pyplot as plt
import numpy as np


# x = [tup[0] for tup in sets['train']]
# y = [tup[1] for tup in sets['train']]

# # Question 1:
# K = [0.1, 0.25, 0.5, 0.9]
# # K = [0.9]
# n = 10
# ll = np.zeros([len(K), n])
# for j, k in enumerate(K):
# 	sets, xvals, yvals = collect_sets('data_split.gz', k=1/(1 - k), n=n)
# 	hmm = HMM(xvals, yvals)
# 	for i in range(n):
# 		print('k: ', k, 'iter: ', i + 1)
# 		x = [tup[0] for tup in sets[i]['train']]
# 		y = [tup[1] for tup in sets[i]['train']]
# 		x_test = [tup[0] for tup in sets[i]['test']]
# 		y_test = [tup[1] for tup in sets[i]['test']]
# 		if len(x_test) > 2400:
# 			x_test = x_test[:2400]
# 			y_test = y_test[:2400]
#
# 		t, e = hmm.mle(x, y)
# 		ll[j, i] = hmm.logLikelihood(x_test, y_test, t, e)
#
# mean = np.mean(ll, 1)
# std = np.std(ll, 1)
#
# plt.errorbar(K, mean, std)
# plt.show()

sets, xvals, yvals = collect_sets('data_split.gz', k=10, max_sentences=5000)
hmm = HMM(xvals, yvals)

x = [tup[0] for tup in sets['train']]
y = [tup[1] for tup in sets['train']]
x_test = [tup[0] for tup in sets['test']]
y_test = [tup[1] for tup in sets['test']]

t, e = hmm.mle(x, y)

print('perceptron\n')
w0 = np.random.rand(hmm.sizeX * (hmm.sizeX + hmm.sizeY))
w = hmm.perceptron(x, y, hmm.phi, w0, 0.001)

print('inference\n')

with open('perceptron_result', 'w') as output:
	for i, seq in enumerate(y_test):
		res = hmm.viterbi(seq, t, e)
		res2 = hmm.viterbi2(seq, hmm.phi, w)
		output.write('------------------------------------------------------------------------\n')
		output.write('real x:\t' + str(x_test[i]) + '\n')
		output.write('vit1 x:\t' + str(res) + '\n')
		output.write('vit2 x:\t' + str(res2) + '\n')

# print('real x:', x_test[201])
# print('pred1x:', res)
# print('pred2x:', res2)